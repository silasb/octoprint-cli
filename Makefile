SOURCEDIR=.
SOURCES := $(shell find $(SOURCEDIR) -maxdepth 1 -name '*.go')

BINARY=octoprint-cli

VERSION=1.0.0
BUILD_TIME=`date +%FT%T%z`

# Setup the -ldflags option for go build here, interpolate the variable values
LDFLAGS=-ldflags "-s -w -X main.Version=$(VERSION) -X main.BuildTime=$(BUILD_TIME)"

GOOS ?= linux
GOARCH ?= amd64

EXE := $(BINARY)#-$(GOARCH)
.DEFAULT_GOAL := $(EXE)

$(EXE): $(SOURCES)
	GOOS=$(GOOS) GOARCH=$(GOARCH) go build $(LDFLAGS) -o $@ $(SOURCES)


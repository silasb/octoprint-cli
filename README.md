# octoprint-cli

## WIP - 1/21/2017 - Usable, but be wary

Simple command line client for Octoprint.

```
go get -d gitlab.com/silasb/octoprint-cli
cd $GOPATH/src/gitlab.com/silasb/octoprint-cli
glide install
go test -v gitlab.com/silasb/octoprint-cli/api/integration
go install gitlab.com/silasb/octoprint-cli

# list files
./octoprint-cli --host http://10.5.5.15:5001 --key 1234 files list
# upload file
./octoprint-cli --host http://10.5.5.15:5000 --key 1234 upload examples/test.gcode
```

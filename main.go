package main

import cli "gitlab.com/silasb/octoprint-cli/cli"

var Version string

func main() {
	cli.Main(Version)
}
